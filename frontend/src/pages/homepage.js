import React from "react";
import { Button, Container, Nav } from "reactstrap";
import '../App.css';
import logo from '../images/logo.png'
import ad from '../images/backgroundW.png'
import image from '../images/biggerflatlay.jpg'

const Homepage = () => {
    return <div>
        <Container className="homepageMain">
            <Nav className="nav">
                <ul>
                    <li>Wefund</li>
                    <li>About</li>
                    <li>Blog</li>
                    <li>FAQ</li>
                    <li>Contact</li>
                </ul>
            </Nav>
            
            <section className="mainSection" id="middle">
                <h1>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h1>
                {/* <img src={image} style={{ height: 800, width: 1500 }}></img> */}
            </section>
            {/* <Button className="mainButton"></Button> */}
            <section className="mainAbout"><h4>this is the about section</h4></section>
            <section className="adOne"><img src={logo} style={{ height: 500, width: 300 }}></img></section>
            <section className="adTwo"><img src={logo} style={{ height: 500, width: 300 }}></img></section>
            <section className="adThree"><img src={logo} style={{ height: 500, width: 300 }}></img></section>
            <section className="adFour"><img src={logo} style={{ height: 500, width: 300 }}></img></section>
            <section className="infoOne">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</section>
            <section className="infoImageOne"></section>
            <section className="infoTwo">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</section>
            <section className="infoImageTwo"></section>
        </Container>
        <footer></footer>
    </div>
}

export default Homepage