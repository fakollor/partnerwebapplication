import React from "react";
import { Grid, Box, Avatar, Paper, Card, CardActions, CardContent, Button, Typography } from '@material-ui/core';
import logo from '../images/logo.png'
import avatarA from '../images/what.png'
import '../App.css';


  const card = (
    <React.Fragment>
      <CardContent>
        <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
          Created by 
          {/* {createdUser} */}
        </Typography>
        <Typography variant="h5" component="div" divider>
          Name of Cluster
          {/* {clusterName} */}
        </Typography>
        <Typography variant="body2">
          Period Time:
          {/* {wefundLength}  */}
          <br />
          Start Date: 
          {/* {startDate}  */}
          <br />
          Draw Amount:
          {/* {drawAmount} */}
          <br />
          Next Draw Date: 
          {/* {drawDate} */}
          <br />
          A Note from the Admin:
          {/* {adminNote} */}
        </Typography>
      </CardContent>
      <CardActions>
        <Button size="small">Invite</Button>
      </CardActions>
    </React.Fragment>
  );

const ClientLandingPage = () => {
    return <div>
        <Grid container>
            <Grid item xs={2}>
                <Paper className="profileBar">
                    <img src={logo} className="menuLogo" style={{ height: 250, width: 250 }}></img>
                    <Avatar alt="Remy Sharp" src={avatarA} style={{ width: 100, height: 100}}/>
                    {/* <h3> {userName} </h3> */}
                </Paper>
            </Grid>
            <Grid item xs={10}>
                {/* <Paper className="menuBar"> */}
                    <nav class="nav">
                        <ul>
                            <li>Wefund</li>
                            <li>About</li>
                            <li>Blog</li>
                            <li>FAQ</li>
                            <li>Contact</li>
                        </ul>
                    </nav>
                    <h1>Your Clusters</h1>
                    <Box sx={{ width: 275 }}>
                        <Card variant="outlined">{card}</Card>
                    </Box>
                {/* </Paper> */}
            </Grid>
        </Grid>
        <Grid>
            {/* <Grid item xs={2}>
                <Box className="menuPane" sx={{ flexGrow: 1,maxWidth: 400}}></Box>
            </Grid> 
            <Grid item xs={8}></Grid>

            <Grid item xs={2}></Grid> */}
        </Grid>
    </div>
}
export default ClientLandingPage