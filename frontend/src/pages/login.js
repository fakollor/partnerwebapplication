import React from 'react';
import '../App.css';
import { Container, Button } from 'reactstrap';
import { Dialog, Grid, Slide } from '@material-ui/core';
import logo from '../images/logo1.png'
import AdminLoginModal from '../components/adminLoginModal';
import LoginModal from '../components/loginModal';
import SignUpModal from '../components/signUpModal';

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
  });

const LoginPage = () => {
    
    const [open, setOpen] = React.useState(false);
    const [clientOpen, setClientOpen] = React.useState(false);
    const [signUpOpen, setSignUpOpen] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    }; 

    const handleClose = () => {
        setOpen(false);
    };
    
    const clientHandleClickOpen = () => {
        setClientOpen(true);
    }; 

    const clientHandleClose = () => {
        setClientOpen(false);
    };

    const signUpHandleClickOpen = () => {
        setSignUpOpen(true);
    }; 

    const signUpHandleClose = () => {
        setSignUpOpen(false);
    };
    
    return <div>
    
        <Container>
            <div className="row expanded top">
                <div className="column small-2">
                    <ul className="doodle">
                        <li className="doodleCir"></li>
                        <li className="doodleCir"></li>
                        <li className="doodleCir"></li>
                    </ul>
                </div>  
            </div>
        </Container>

        <Container className="loginMenu">
            <Grid container direction="column" justifyContent="space-between" alignItems="center" className="gridCenter">
                <Grid>
                    <img 
                    // onClick={() => togglePopup(true)}
                    style={{ height: 500, width: 500 }}
                    src={logo} />
                </Grid>
                <Grid className="listItem">
                    <Button className="adminLogin" onClick={handleClickOpen}>Admin Login</Button>
                </Grid>
                <Grid className="listItem">
                    <Button className="clientLogin" onClick={clientHandleClickOpen}>Client Login</Button>
                </Grid>
                <Grid className="listItem">
                    <Button className="signUp" onClick={signUpHandleClickOpen}>Sign Up</Button>
                </Grid>
            </Grid>
        </Container>

        <Dialog style={{ zIndex: 5000 }} open={open} TransitionComponent={Transition} onClose={handleClose}>
            <AdminLoginModal />
        </Dialog>
        <Dialog style={{ zIndex: 5000 }} open={clientOpen} TransitionComponent={Transition} onClose={clientHandleClose}>
            <LoginModal />
        </Dialog>
        <Dialog style={{ zIndex: 5000 }} open={signUpOpen} TransitionComponent={Transition} onClose={signUpHandleClose}>
            <SignUpModal />
        </Dialog>

    </div>
};

export default LoginPage