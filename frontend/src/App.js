import React, {Component} from 'react';
import './App.css';
import { 
  BrowserRouter as Router,
  Route, 
  Switch 
} from 'react-router-dom';
import LoginPage from './pages/login';
import Homepage from './pages/homepage';
import ClientLandingPage from './pages/clientLandingPage'
import AdminLandingPage from './pages/adminLandingPage'


class App extends Component {
  
  render() {
  
    return (
      <Router>
        <Switch>
          <Route exact path="/" component={Homepage} />
          <Route exact path="/login" component={LoginPage} />
          <Route exact path="/clientPage" component={ClientLandingPage} />
          <Route exact path="/adminPage" component={AdminLandingPage} />
        </Switch>
      </Router>
    )
  }
}


export default App;
