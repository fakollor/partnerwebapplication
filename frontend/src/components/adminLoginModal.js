import { makeStyles, TextField, Link, Grid } from '@material-ui/core';
import React, {useState} from 'react';
import { Button, Container } from 'reactstrap';
import '../App.css';

const AdminLoginModal = () => {

    const useStyles = makeStyles({
        root: {
            width: "600px",
            height:"65vh",
            margin: "0 auto",
        },
        form: {
            paddingLeft: "100px",
            paddingRight: "100px",
        },
        button: {
            width: "400px",
            marginTop: "30px",
            padding: "8px",
            color: "#114542",
            backgroundColor: "#e4bbad",
            borderRadius: "8px"
        },
        buttonLink: {
            width: "400px",
            // marginTop: "30px",
            padding: "8px",
            backgroundColor: "#fff",
            borderRadius: "8px"
        }
    });

    const classes = useStyles();
    const [userId, setUserId] = useState("")
    const [password, setPassword] = useState("")
    const [auth, setAuth] = useState(true)
    

    // const adminLoginSubmit = async event => {

    //     event.preventDefault()
    //     const response = await fetch('http://localhost:3000/auth', {
    //         method: 'POST',
    //         headers: {
    //             'Accept': 'application/json',
    //             'Content-Type': 'application/json'
    //         },
    //         body: JSON.stringify({userId, password})
    //     })
    //     const payload = await response.json()
    //     if (response.status >= 400) {
    //         setAuth(false)
    //     } else {
    //         sessionStorage.setItem('token', payload.token)

    //         history.push("/userPortal");

    //         let { from } = location.state || { from: { pathname: "/UserPortal" } };
    //         history.replace(from);
    //     }
    // }

    

    return <div>
        {/* <Paper elevation={3} 
        className={classes.root}
        > */}
            <Container className="signInPaper">
                <form className={classes.form}
                // onSubmit={adminLoginSubmit}
                >
                    <Grid container direction="column" justifyContent="center" alignItems="center">
                        <h4>Welcome</h4>
                        <TextField
                            required
                            fullWidth
                            id="outlined secondary"
                            label="Username or Email"
                            color="pink"
                            value={userId}
                            onChange={event => setUserId(event.target.value)}
                            />
                        <Button className={classes.button}>
                            Continue
                        </Button>
                        <h6>OR</h6>
                        <Button className={classes.buttonLink}>
                            Continue with Google
                        </Button>
                        <h5>Don't have an account? <Link> Sign Up</Link></h5>
                        <Link>Forgot Password?</Link>
                    </Grid>
                </form>
            </Container>
        {/* </Paper> */}
    </div>

};

export default AdminLoginModal